# frozen_string_literal: true

module Analytics
  class FoundationsController < ApplicationController
    before_action :find_company

    def index
      base_breadcrumbs

      @grid = Analytics::FoundationsGrid.new(grid_params) do |scope|
        scope.where(company_id: @company.id).page(params[:page])
      end
    end

    def calculate
      Analytics::Foundations::CalculateAllService.call(@company)
      redirect_to(action: :index, company_id: @company.id)
    end

    private

    def grid_params
      params.fetch(:analytics_foundations_grid, {}).permit!
    end

    def find_company
      @company = Company.find(params[:company_id])
    end

    def base_breadcrumbs
      breadcrumb I18n.t('companies'), companies_path
      breadcrumb @company.name, company_path(@company.id)
      breadcrumb I18n.t('analytics_foundations'), company_analytics_foundations_path(@company.id)
    end
  end
end
