# frozen_string_literal: true

module Analytics
  class RatiosController < ApplicationController
    before_action :find_company

    def index
      base_breadcrumbs

      @grid = Analytics::RatiosGrid.new(grid_params) do |scope|
        scope.where(company_id: @company.id).page(params[:page])
      end
    end

    def calculate
      Analytics::Ratios::CalculateAllService.call(@company)
      redirect_to(action: :index, company_id: @company.id)
    end

    private

    def grid_params
      params.fetch(:analytics_ratios_grid, {}).permit!
    end

    def find_company
      @company = Company.find(params[:company_id])
    end

    def base_breadcrumbs
      breadcrumb I18n.t('companies'), companies_path
      breadcrumb @company.name, company_path(@company.id)
      breadcrumb I18n.t('analytics_ratios'), company_analytics_ratios_path(@company.id)
    end
  end
end
