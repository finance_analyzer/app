# frozen_string_literal: true

module Analytics
  class ResultsController < ApplicationController
    before_action :find_company

    def calculate
      Analytics::Results::CalculateAllService.call(@company)
      redirect_to(company_path(@company))
    end

    private

    def find_company
      @company = Company.find(params[:company_id])
    end
  end
end
