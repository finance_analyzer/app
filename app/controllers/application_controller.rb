# frozen_string_literal: true

class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: ENV['SELF_USERNAME'], password: ENV['SELF_PASSWORD']
end
