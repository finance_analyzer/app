# frozen_string_literal: true

class CompaniesController < ApplicationController
  breadcrumb I18n.t('companies'), :companies_path

  before_action :find_company, only: %i[show edit update destroy parse_and_calculate calculate]
  before_action :perform_select_options, only: %i[new edit create update]

  def index
    @grid = CompaniesGrid.new(grid_params) do |scope|
      scope.page(params[:page])
    end
  end

  def new
    @company = Company.new

    breadcrumb I18n.t('form.create'), new_company_path
  end

  def show
    breadcrumb @company.name, company_path(@company.id)
  end

  def create
    @company = Company.new(company_params)
    @company.save!
    Companies::SetConomyUrlWorker.perform_async(@company.id)
    redirect_to(action: :show, id: @company.id)
  rescue ActiveRecord::RecordInvalid
    render(:new)
  end

  def edit
    breadcrumb "#{I18n.t('form.edit')} #{@company.name}", edit_company_path(@company.id)
  end

  def update
    @company.update!(company_params)
    redirect_to(action: :show, id: @company.id)
  rescue ActiveRecord::RecordInvalid
    render(:edit)
  end

  def destroy
    @company.destroy!
    redirect_to(action: :index)
  end

  def parse_and_calculate
    Ifrs::ComprehensiveIncomes::ParserWorker.perform_async(@company.id, true)
    Ifrs::FinancialPositions::ParserWorker.perform_async(@company.id, true)
    Shares::ParserWorker.perform_in(1.minute, @company.id, true)

    redirect_to(action: :show, id: @company.id)
  end

  def calculate
    Analytics::CalculateAllService.call(@company)

    redirect_to(action: :show, id: @company.id)
  end

  private

  def grid_params
    params.fetch(:companies_grid, {}).permit!
  end

  def find_company
    @company = Company.find(params[:id] || params[:company_id])
  end

  def company_params
    params.require(:company)
          .permit(:name, :index, :country_id, :industry_id, :finam_em_id)
          .reject { |_key, value| value.blank? }
  end

  def perform_select_options
    @countries = Country.select_options
    @industries = Industry.select_options
    @finam_ems = Finam::Em.select_options
  end
end
