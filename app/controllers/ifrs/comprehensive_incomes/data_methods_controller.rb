# frozen_string_literal: true

module Ifrs
  module ComprehensiveIncomes
    class DataMethodsController < ApplicationController
      breadcrumb I18n.t('ci_data_methods'), :ifrs_comprehensive_incomes_data_methods_path

      before_action :find_data_method, only: %i[edit update destroy]
      before_action :perform_select_options, only: %i[new edit create update]

      def index
        @grid = Ifrs::ComprehensiveIncomes::DataMethodsGrid.new(grid_params) do |scope|
          scope.page(params[:page])
        end
      end

      def new
        @data_method = Ifrs::ComprehensiveIncomes::DataMethod.new

        breadcrumb I18n.t('form.create'), new_ifrs_comprehensive_incomes_data_method_path
      end

      def create
        Ifrs::ComprehensiveIncomes::DataMethod.create!(data_method_params)
        redirect_to(action: :index)
      end

      def edit
        breadcrumb("#{I18n.t('form.edit')} #{@data_method.value}",
                   edit_ifrs_comprehensive_incomes_data_method_path(@data_method.id))
      end

      def update
        @data_method.update!(data_method_params)
        redirect_to(action: :index)
      end

      def destroy
        @data_method.destroy!
        redirect_to(action: :index)
      end

      private

      def grid_params
        params.fetch(:companies_grid, {}).permit!
      end

      def find_data_method
        @data_method = Ifrs::ComprehensiveIncomes::DataMethod.find(params[:id])
      end

      def data_method_params
        params.require(:ifrs_comprehensive_incomes_data_method).permit(:key, :value)
      end

      def perform_select_options
        @keys = Ifrs::ComprehensiveIncomes::DataMethod.select_options
      end
    end
  end
end
