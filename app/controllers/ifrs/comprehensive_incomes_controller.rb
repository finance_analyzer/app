# frozen_string_literal: true

module Ifrs
  class ComprehensiveIncomesController < ApplicationController
    before_action :find_company

    def index
      base_breadcrumbs

      @grid = Ifrs::ComprehensiveIncomesGrid.new(grid_params) do |scope|
        scope.where(company_id: @company.id).page(params[:page])
      end
    end

    def parse
      Ifrs::ComprehensiveIncomes::ParserWorker.perform_async(@company.id)
      redirect_to(action: :index, company_id: @company.id)
    end

    private

    def grid_params
      params.fetch(:ifrs_comprehensive_incomes_grid, {}).permit!
    end

    def find_company
      @company = Company.find(params[:company_id])
    end

    def base_breadcrumbs
      breadcrumb I18n.t('companies'), companies_path
      breadcrumb @company.name, company_path(@company.id)
      breadcrumb I18n.t('ifrs_comprehensive_incomes'), company_ifrs_comprehensive_incomes_path(@company.id)
    end
  end
end
