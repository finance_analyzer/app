# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    class DataMethodsController < ApplicationController
      breadcrumb I18n.t('fp_data_methods'), :ifrs_financial_positions_data_methods_path

      before_action :find_data_method, only: %i[edit update destroy]
      before_action :perform_select_options, only: %i[new edit create update]

      def index
        @grid = Ifrs::FinancialPositions::DataMethodsGrid.new(grid_params) do |scope|
          scope.page(params[:page])
        end
      end

      def new
        @data_method = Ifrs::FinancialPositions::DataMethod.new

        breadcrumb I18n.t('form.create'), new_ifrs_financial_positions_data_method_path
      end

      def create
        Ifrs::FinancialPositions::DataMethod.create!(data_method_params)
        redirect_to(action: :index)
      end

      def edit
        breadcrumb("#{I18n.t('form.edit')} #{@data_method.value}",
                   edit_ifrs_financial_positions_data_method_path(@data_method.id))
      end

      def update
        @data_method.update!(data_method_params)
        redirect_to(action: :index)
      end

      def destroy
        @data_method.destroy!
        redirect_to(action: :index)
      end

      private

      def grid_params
        params.fetch(:companies_grid, {}).permit!
      end

      def find_data_method
        @data_method = Ifrs::FinancialPositions::DataMethod.find(params[:id])
      end

      def data_method_params
        params.require(:ifrs_financial_positions_data_method).permit(:key, :value)
      end

      def perform_select_options
        @keys = Ifrs::FinancialPositions::DataMethod.select_options
      end
    end
  end
end
