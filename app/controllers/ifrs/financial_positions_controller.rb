# frozen_string_literal: true

module Ifrs
  class FinancialPositionsController < ApplicationController
    before_action :find_company

    def index
      base_breadcrumbs

      @grid = Ifrs::FinancialPositionsGrid.new(grid_params) do |scope|
        scope.where(company_id: @company.id).page(params[:page])
      end
    end

    def parse
      Ifrs::FinancialPositions::ParserWorker.perform_async(@company.id)
      redirect_to(action: :index, company_id: @company.id)
    end

    private

    def grid_params
      params.fetch(:ifrs_financial_positions_grid, {}).permit!
    end

    def find_company
      @company = Company.find(params[:company_id])
    end

    def base_breadcrumbs
      breadcrumb I18n.t('companies'), companies_path
      breadcrumb @company.name, company_path(@company.id)
      breadcrumb I18n.t('ifrs_financial_positions'), company_ifrs_financial_positions_path(@company.id)
    end
  end
end
