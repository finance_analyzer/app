# frozen_string_literal: true

class SharesController < ApplicationController
  before_action :find_company
  before_action :find_share, only: %i[edit update destroy]

  def index
    base_breadcrumbs

    @grid = SharesGrid.new(grid_params) do |scope|
      scope.where(company_id: @company.id).page(params[:page])
    end
  end

  def edit
    base_breadcrumbs
    breadcrumb "#{I18n.t('form.edit')} #{@share.id}", edit_company_share_path(@company.id, @share.id)
  end

  def update
    @share.update!(share_params)
    redirect_to(action: :index, company_id: @company.id)
  end

  def new
    @share = @company.shares.new

    base_breadcrumbs
    breadcrumb I18n.t('form.create'), new_company_share_path(@company.id)
  end

  def create
    share = Share.create!(share_params.merge(company_id: @company.id))
    redirect_to(action: :index, company_id: @company.id, id: share.id)
  end

  def destroy
    @share.destroy!
    redirect_to(action: :index, company_id: @company.id)
  end

  def parse
    Shares::ParserWorker.perform_async(@company.id)
    redirect_to(action: :index, company_id: @company.id)
  end

  private

  def share_params
    params.require(:share).permit(:amount, :value, :date)
  end

  def grid_params
    params.fetch(:shares_grid, {}).permit!
  end

  def find_company
    @company = Company.find(params[:company_id])
  end

  def find_share
    @share = @company.shares.find(params[:id])
  end

  def base_breadcrumbs
    breadcrumb I18n.t('companies'), companies_path
    breadcrumb @company.name, company_path(@company.id)
    breadcrumb I18n.t('shares'), company_shares_path(@company.id)
  end
end
