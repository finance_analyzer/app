# frozen_string_literal: true

class TotalAnalyticsController < ApplicationController
  breadcrumb I18n.t('total_analytics_name'), :total_analytics_path

  before_action :perform_select_options, only: %i[index]
  before_action :perform_input_values, only: %i[index]

  # rubocop:disable Metrics/MethodLength
  def index
    @keys = Analytics::Results::CalculateService::ATTRS
    @companies_names = companies.each_with_object({}) do |company, result|
      result[company.id.to_s] = company.name
    end
    @companies_analytics = (start_year..end_year).map do |year|
      {
        year: year,
        analytics: companies.each_with_object({}) do |company, result|
          result[company.id.to_s] = company.analytics_results.find { |ar| ar.date.year == year }
        end,
      }
    end.reverse
  end
  # rubocop:enable Metrics/MethodLength

  def calculate
    Company.all.each do |company|
      Analytics::CalculateAllWorker.perform_async(company.id)
    end
    redirect_to(action: :index)
  end

  private

  def companies
    @companies ||= Company.with_analytics.where({
      country_id: @country_id,
      industry_id: @industry_id,
    }.select { |_key, value| value.present? })
  end

  def start_year
    @start_year ||= begin
      index_params[:start_date].present? ? Date.parse(index_params[:start_date]).year : Share.minimum(:date).year
    end
  end

  def end_year
    @end_year ||= begin
      index_params[:end_date].present? ? Date.parse(index_params[:end_date]).year : Share.maximum(:date).year
    end
  end

  def index_params
    @index_params ||= params.permit(:country_id, :industry_id, :start_date, :end_date)
  end

  def perform_input_values
    @country_id = index_params[:country_id] || Country.first.id
    @industry_id = index_params[:industry_id] || Industry.first.id
  end

  def perform_select_options
    @countries = Country.select_options
    @industries = Industry.select_options
    @dates = begin
               (Share.minimum(:date).year..Share.maximum(:date).year).map do |year|
                 ["31.12.#{year}", "31.12.#{year}"]
               end
             rescue NoMethodError
               ['', '']
             end
  end
end
