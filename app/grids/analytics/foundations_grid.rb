# frozen_string_literal: true

module Analytics
  class FoundationsGrid < BaseGrid
    scope do
      Analytics::Foundation
    end

    column(:id, header: 'ID')
    date_column(:date, header: I18n.t('date'))
    percentage(:roe, header: I18n.t('analytics.foundations.roe'))
    readable_number(:total_market_capitalization, header: I18n.t('analytics.foundations.total_market_capitalization'))
    percentage(:avg_net_profit_inc_for_this_year,
               header: I18n.t('analytics.foundations.avg_net_profit_inc_for_this_year'))
    percentage(:avg_net_profit_inc_for_all_years,
               header: I18n.t('analytics.foundations.avg_net_profit_inc_for_all_years'))
    readable_number(:net_profit_per_share,
                    header: I18n.t('analytics.foundations.net_profit_per_share'))
    percentage(:avg_net_profit_inc_per_share_for_this_year,
               header: I18n.t('analytics.foundations.avg_net_profit_inc_per_share_for_this_year'))
    percentage(:avg_net_profit_inc_per_share_for_all_years,
               header: I18n.t('analytics.foundations.avg_net_profit_inc_per_share_for_all_years'))
    readable_number(:working_capital, header: I18n.t('analytics.foundations.working_capital'))
    readable_number(:balance_share_price, header: I18n.t('analytics.foundations.balance_share_price'))
  end
end
