# frozen_string_literal: true

module Analytics
  class RatiosGrid < BaseGrid
    scope do
      Analytics::Ratio
    end

    column(:id, header: 'ID')
    date_column(:date, header: I18n.t('date'))
    readable_number(:share_price_per_net_profit, header: I18n.t('analytics.ratios.share_price_per_net_profit'))
    readable_number(:share_price_per_balance_price, header: I18n.t('analytics.ratios.share_price_per_balance_price'))
    percentage(:net_profit_per_revenue, header: I18n.t('analytics.ratios.net_profit_per_revenue'))
    percentage(:net_profit_per_balance_price, header: I18n.t('analytics.ratios.net_profit_per_balance_price'))
    readable_number(:working_capital_per_liabilities,
                    header: I18n.t('analytics.ratios.working_capital_per_liabilities'))
    readable_number(:market_capitalization_per_revenue,
                    header: I18n.t('analytics.ratios.market_capitalization_per_revenue'))
    readable_number(:current_assets_per_current_liabilities,
                    header: I18n.t('analytics.ratios.current_assets_per_current_liabilities'))
    readable_number(:net_profit_per_liabilities, header: I18n.t('analytics.ratios.net_profit_per_liabilities'))
  end
end
