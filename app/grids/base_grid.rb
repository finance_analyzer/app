# frozen_string_literal: true

class BaseGrid
  include Datagrid

  self.default_column_options = {
    # Uncomment to disable the default order
    # order: false,
    # Uncomment to make all columns HTML by default
    # html: true,
  }
  # Enable forbidden attributes protection
  # self.forbidden_attributes_protection = true

  def self.date_column(name, *args)
    column(name, *args) do |model|
      format(block_given? ? yield : model.send(name)) do |date|
        date.strftime('%d.%m.%Y')
      end
    end
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def self.readable_number(name, *args)
    column(name, *args) do |model|
      format(block_given? ? yield : model.send(name)) do |number|
        if number.nil?
          nil
        else
          last_half = number.to_s.include?('.') ? ".#{number.to_s.split('.').last[0..1]}" : ''
          result = number.to_s.split('.')[0].reverse
          (result.length / 3).times do |time|
            result.insert((time + 1) * 3 + time, ' ')
          end
          result = result[0..-2] if result.ends_with?(' ')
          result.reverse + last_half
        end
      end
    end
  end

  def self.percentage(name, *args)
    column(name, *args) do |model|
      format(block_given? ? yield : model.send(name)) do |number|
        if number.nil?
          nil
        else
          last_half = number.to_s.include?('.') ? ".#{number.to_s.split('.').last[0..1]}" : ''
          result = number.to_s.split('.')[0].reverse
          (result.length / 3).times do |time|
            result.insert((time + 1) * 3 + time, ' ')
          end
          result = result[0..-2] if result.ends_with?(' ')
          result.reverse + last_half + '%'
        end
      end
    end
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength
end
