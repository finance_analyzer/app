# frozen_string_literal: true

class CompaniesGrid < BaseGrid
  scope do
    Company.order(:name)
  end

  filter(:name, :string, header: I18n.t('company.name'))
  filter(:created_at, :date, range: true, header: I18n.t('record.created_at'))

  column(:id, header: 'ID')
  column(:name, html: true, header: I18n.t('company.name')) do |company|
    link_to(company.name, company_path(company.id))
  end
  date_column(:created_at, header: I18n.t('record.created_at'))
  column(:edit, html: true, header: I18n.t('form.edit')) do |company|
    link_to(I18n.t('form.edit'), edit_company_path(company.id))
  end
  column(:delete, html: true, header: I18n.t('form.delete')) do |company|
    link_to(I18n.t('form.delete'), company_path(company.id), method: :delete)
  end
end
