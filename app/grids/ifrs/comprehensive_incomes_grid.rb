# frozen_string_literal: true

module Ifrs
  class ComprehensiveIncomesGrid < BaseGrid
    scope do
      Ifrs::ComprehensiveIncome
    end

    column(:id, header: 'ID')
    date_column(:date, header: I18n.t('date'))
    readable_number(:revenue, header: I18n.t('ifrs.comprehensive_incomes.revenue'))
    readable_number(:net_profit, header: I18n.t('ifrs.comprehensive_incomes.net_profit'))
  end
end
