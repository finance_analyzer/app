# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    class DataMethodsGrid < BaseGrid
      scope do
        Ifrs::FinancialPositions::DataMethod
      end

      column(:id, header: 'ID')
      column(:key, header: I18n.t('key'))
      column(:value, header: I18n.t('value'))
      date_column(:created_at, header: I18n.t('record.created_at'))
      column(:edit, html: true, header: I18n.t('form.edit')) do |data_method|
        link_to(I18n.t('form.edit'), edit_ifrs_financial_positions_data_method_path(data_method.id))
      end
      column(:delete, html: true, header: I18n.t('form.delete')) do |data_method|
        link_to(I18n.t('form.delete'), ifrs_financial_positions_data_method_path(data_method.id), method: :delete)
      end
    end
  end
end
