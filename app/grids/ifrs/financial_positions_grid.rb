# frozen_string_literal: true

module Ifrs
  class FinancialPositionsGrid < BaseGrid
    scope do
      Ifrs::FinancialPosition
    end

    column(:id, header: 'ID')
    date_column(:date, header: I18n.t('date'))
    readable_number(:non_current_assets, header: I18n.t('ifrs.financial_positions.non_current_assets'))
    readable_number(:current_assets, header: I18n.t('ifrs.financial_positions.current_assets'))
    readable_number(:total_assets, header: I18n.t('ifrs.financial_positions.total_assets'))
    readable_number(:equity, header: I18n.t('ifrs.financial_positions.equity'))
    readable_number(:non_current_liabilities, header: I18n.t('ifrs.financial_positions.non_current_liabilities'))
    readable_number(:current_liabilities, header: I18n.t('ifrs.financial_positions.current_liabilities'))
    readable_number(:total_liabilities, header: I18n.t('ifrs.financial_positions.total_liabilities'))
    readable_number(:dividend_per_share, header: I18n.t('ifrs.financial_positions.dividend_per_share'))
  end
end
