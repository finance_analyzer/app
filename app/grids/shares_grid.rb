# frozen_string_literal: true

class SharesGrid < BaseGrid
  scope do
    Share
  end

  column(:id, header: 'ID')
  date_column(:date, header: I18n.t('date'))
  readable_number(:amount, header: I18n.t('share.amount'))
  readable_number(:value, header: I18n.t('share.value'))
  column(:edit, html: true, header: I18n.t('form.edit')) do |share|
    link_to(I18n.t('form.edit'), edit_company_share_path(share.company_id, share.id))
  end
  column(:delete, html: true, header: I18n.t('form.delete')) do |share|
    link_to(I18n.t('form.delete'), company_share_path(share.company_id, share.id), method: :delete)
  end
end
