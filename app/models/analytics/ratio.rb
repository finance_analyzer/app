# frozen_string_literal: true

module Analytics
  class Ratio < ApplicationRecord
    self.table_name_prefix = 'analytics_'

    default_scope { order(date: :desc) }

    belongs_to :company
  end
end
