# frozen_string_literal: true

module Analytics
  class Result < ApplicationRecord
    self.table_name_prefix = 'analytics_'

    belongs_to :company
  end
end
