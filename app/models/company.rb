# frozen_string_literal: true

class Company < ApplicationRecord
  belongs_to :country
  belongs_to :industry
  belongs_to :finam_em, class_name: 'Finam::Em'

  has_many :ifrs_financial_positions, class_name: 'Ifrs::FinancialPosition'
  has_many :ifrs_comprehensive_incomes, class_name: 'Ifrs::ComprehensiveIncome'
  has_many :shares
  has_many :analytics_foundations, class_name: 'Analytics::Foundation'
  has_many :analytics_ratios, class_name: 'Analytics::Ratio'
  has_many :analytics_results, class_name: 'Analytics::Result'

  scope :with_analytics, -> { joins(:analytics_foundations, :analytics_ratios) }

  validates :name, presence: true, uniqueness: true
  validates :index, presence: true, uniqueness: true

  def ready_for_analytics_calculation?
    conomy_url.present? &&
      ifrs_financial_positions.present? &&
      ifrs_comprehensive_incomes.present? &&
      shares.present?
  end

  def perform_calculating
    return unless ready_for_analytics_calculation?

    Analytics::CalculateAllWorker.perform_async(id)
  end
end
