# frozen_string_literal: true

class Country < ApplicationRecord
  default_scope { order(:name) }

  validates :name, presence: true, uniqueness: true

  def self.select_options
    Country.pluck(:name, :id)
  end
end
