# frozen_string_literal: true

module Finam
  class Em < ApplicationRecord
    self.table_name_prefix = 'finam_'

    default_scope { order(name: :asc) }

    def self.select_options
      Finam::Em.pluck(:name, :id)
    end
  end
end
