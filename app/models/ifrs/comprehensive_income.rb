# frozen_string_literal: true

class Ifrs::ComprehensiveIncome < ApplicationRecord
  self.table_name_prefix = 'ifrs_'

  default_scope { order(date: :desc) }

  belongs_to :company
end
