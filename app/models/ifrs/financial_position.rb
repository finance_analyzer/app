# frozen_string_literal: true

module Ifrs
  class FinancialPosition < ApplicationRecord
    self.table_name_prefix = 'ifrs_'

    default_scope { order(date: :desc) }

    belongs_to :company
  end
end
