# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    class DataMethod < ApplicationRecord
      self.table_name_prefix = 'ifrs_financial_positions_'

      def self.select_options
        Ifrs::FinancialPositions::TableParsers::GeneralService::DATA_METHODS.values.uniq
      end

      def self.to_hash
        all.each_with_object({}) do |data_method, result|
          result[data_method.value] = data_method.key.to_sym
        end
      end
    end
  end
end
