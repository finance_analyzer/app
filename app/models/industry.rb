# frozen_string_literal: true

class Industry < ApplicationRecord
  default_scope { order(:name) }

  validates :name, presence: true, uniqueness: true

  def self.select_options
    Industry.pluck(:name, :id)
  end
end
