# frozen_string_literal: true

class Share < ApplicationRecord
  default_scope { order(date: :desc) }

  belongs_to :company
end
