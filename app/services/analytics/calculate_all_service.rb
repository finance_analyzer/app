# frozen_string_literal: true

module Analytics
  class CalculateAllService < ApplicationService
    Contract ClassName[Company] => Any
    def initialize(company)
      @company = company
    end

    Contract Any
    def call
      ::Analytics::Foundations::CalculateAllService.call(company)
      ::Analytics::Ratios::CalculateAllService.call(company)
      ::Analytics::Results::CalculateAllService.call(company)
    end

    private

    attr_reader :company
  end
end
