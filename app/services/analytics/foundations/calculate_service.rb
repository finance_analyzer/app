# frozen_string_literal: true

module Analytics
  module Foundations
    class CalculateService < ApplicationService
      Contract ClassName[Share] => Any
      def initialize(share)
        @share = share
      end

      Contract Any
      def call
        return unless share_valid?

        analytics_foundation = share.company.analytics_foundations.find_or_initialize_by(date: share.date)
        analytics_foundation.assign_attributes(analytics_foundation_attributes)
        analytics_foundation.save!
      end

      private

      ROUND_VALUE = 4

      ATTRS = %i[
        roe
        total_market_capitalization
        avg_net_profit_inc_for_this_year
        avg_net_profit_inc_for_all_years
        net_profit_per_share
        avg_net_profit_inc_per_share_for_this_year
        avg_net_profit_inc_per_share_for_all_years
        working_capital
        balance_share_price
      ].freeze

      attr_reader :share

      def share_valid?
        ifrs_financial_position.present? && ifrs_comprehensive_income.present?
      end

      def analytics_foundation_attributes
        @analytics_foundation_attributes ||= begin
          result = {
            date: share.date,
          }
          ATTRS.each do |attr|
            result[attr] = send(attr)
          end
          result
        end
      end

      def ifrs_financial_position
        @ifrs_financial_position ||= share.company.ifrs_financial_positions.find_by(date: share.date)
      end

      def ifrs_comprehensive_income
        @ifrs_comprehensive_income ||= share.company.ifrs_comprehensive_incomes.find_by(date: share.date)
      end

      def prev_ifrs_comprehensive_income
        @prev_ifrs_comprehensive_income ||= begin
          share.company.ifrs_comprehensive_incomes.find_by(date: share.date - 1.year)
        end
      end

      # rubocop:disable Metrics/MethodLength
      def prev_foundations
        @analytics_foundations ||= begin
          result = []
          foundations = share.company.analytics_foundations.where('date < ?', share.date).order(:date)
          target_date = share.date - 1.year
          loop do
            target = foundations.find_by(date: target_date)
            target_date -= 1.year
            break if target.nil?

            result << target
          end
          result
        end
      end
      # rubocop:enable Metrics/MethodLength

      def prev_foundation
        @prev_foundation ||= share.company.analytics_foundations.find_by(date: share.date - 1.year)
      end

      def roe
        @roe ||= (ifrs_comprehensive_income.net_profit / ifrs_financial_position.equity.to_f * 100).round(ROUND_VALUE)
      end

      def total_market_capitalization
        @total_market_capitalization ||= share.amount * share.value
      end

      def avg_net_profit_inc_for_this_year
        @avg_net_profit_inc_for_this_year ||= begin
          return nil if prev_ifrs_comprehensive_income.nil?

          (
            (
              ifrs_comprehensive_income.net_profit.to_f / prev_ifrs_comprehensive_income.net_profit
            ) * 100 - 100
          ).round(ROUND_VALUE)
        end
      end

      def avg_net_profit_inc_for_all_years
        @avg_net_profit_inc_for_all_years ||= begin
          temp = [
            prev_foundations.pluck(:avg_net_profit_inc_for_this_year),
            avg_net_profit_inc_for_this_year
          ].flatten.compact
          return nil if temp.blank?

          (temp.sum.to_f / prev_foundations.count).round(ROUND_VALUE)
        end
      end

      def net_profit_per_share
        @net_profit_per_share ||= (ifrs_comprehensive_income.net_profit / share.amount.to_f).round(ROUND_VALUE)
      end

      def avg_net_profit_inc_per_share_for_this_year
        @avg_net_profit_inc_per_share_for_this_year ||= begin
          return nil if prev_foundation.nil?

          ((net_profit_per_share.to_f / prev_foundation.net_profit_per_share) * 100 - 100).round(ROUND_VALUE)
        end
      end

      def avg_net_profit_inc_per_share_for_all_years
        @avg_net_profit_inc_per_share_for_all_years ||= begin
          temp = [
            prev_foundations.pluck(:avg_net_profit_inc_per_share_for_this_year),
            avg_net_profit_inc_per_share_for_this_year
          ].flatten.compact
          return nil if temp.blank?

          (temp.sum.to_f / prev_foundations.count).round(ROUND_VALUE)
        end
      end

      def working_capital
        @working_capital ||= ifrs_financial_position.current_assets - ifrs_financial_position.current_liabilities
      end

      def balance_share_price
        @balance_share_price ||= (ifrs_financial_position.equity / share.amount.to_f).round(ROUND_VALUE)
      end
    end
  end
end
