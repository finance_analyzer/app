# frozen_string_literal: true

module Analytics
  module Ratios
    class CalculateAllService < ApplicationService
      Contract ClassName[Company] => Any
      def initialize(company)
        @company = company
      end

      def call
        Analytics::Ratio.transaction do
          company.shares.sort_by(&:date).each do |share|
            CalculateService.call(share)
          end
        end
      end

      private

      attr_reader :company
    end
  end
end
