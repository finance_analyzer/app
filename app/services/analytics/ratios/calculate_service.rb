# frozen_string_literal: true

module Analytics
  module Ratios
    class CalculateService < ApplicationService
      Contract ClassName[Share] => Any
      def initialize(share)
        @share = share
      end

      Contract Any
      def call
        return unless share_valid?

        analytics_ratio = share.company.analytics_ratios.find_or_initialize_by(date: share.date)
        analytics_ratio.assign_attributes(analytics_ratio_attributes)
        analytics_ratio.save!
      end

      private

      ROUND_VALUE = 4

      ATTRS = %i[
        share_price_per_net_profit
        share_price_per_balance_price
        net_profit_per_revenue
        net_profit_per_balance_price
        working_capital_per_liabilities
        market_capitalization_per_revenue
        current_assets_per_current_liabilities
        net_profit_per_liabilities
      ].freeze

      attr_reader :share

      def share_valid?
        ifrs_financial_position.present? && ifrs_comprehensive_income.present? && analytics_foundation.present?
      end

      def analytics_ratio_attributes
        @analytics_ratio_attributes ||= begin
          result = {
            date: share.date,
          }
          ATTRS.each do |attr|
            result[attr] = send(attr)
          end
          result
        end
      end

      def ifrs_financial_position
        @ifrs_financial_position ||= share.company.ifrs_financial_positions.find_by(date: share.date)
      end

      def ifrs_comprehensive_income
        @ifrs_comprehensive_income ||= share.company.ifrs_comprehensive_incomes.find_by(date: share.date)
      end

      def analytics_foundation
        @analytics_foundation ||= share.company.analytics_foundations.find_by(date: share.date)
      end

      def share_price_per_net_profit
        @share_price_per_net_profit ||= begin
          (share.value.to_f / analytics_foundation.net_profit_per_share).round(ROUND_VALUE)
        end
      end

      def share_price_per_balance_price
        @share_price_per_balance_price ||= begin
          (share.value.to_f / analytics_foundation.balance_share_price).round(ROUND_VALUE)
        end
      end

      def net_profit_per_revenue
        @net_profit_per_revenue ||= begin
          (ifrs_comprehensive_income.net_profit.to_f / ifrs_comprehensive_income.revenue * 100).round(ROUND_VALUE)
        end
      end

      def net_profit_per_balance_price
        @net_profit_per_balance_price ||= begin
          (ifrs_comprehensive_income.net_profit.to_f / ifrs_financial_position.equity * 100).round(ROUND_VALUE)
        end
      end

      def working_capital_per_liabilities
        @working_capital_per_liabilities ||= begin
          (analytics_foundation.working_capital.to_f / ifrs_financial_position.total_liabilities).round(ROUND_VALUE)
        end
      end

      def market_capitalization_per_revenue
        @market_capitalization_per_revenue ||= begin
          (
            analytics_foundation.total_market_capitalization.to_f / ifrs_comprehensive_income.revenue
          ).round(ROUND_VALUE)
        end
      end

      def current_assets_per_current_liabilities
        @current_assets_per_current_liabilities ||= begin
          (
            ifrs_financial_position.current_assets.to_f / ifrs_financial_position.current_liabilities
          ).round(ROUND_VALUE)
        end
      end

      def net_profit_per_liabilities
        @net_profit_per_liabilities ||= begin
          (ifrs_comprehensive_income.net_profit.to_f / ifrs_financial_position.total_liabilities).round(ROUND_VALUE)
        end
      end
    end
  end
end
