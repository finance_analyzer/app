# frozen_string_literal: true

module Analytics
  module Results
    class CalculateService < ApplicationService
      ATTRS = %i[
        current_assets_per_current_liabilities
        share_price_per_net_profit
        avg_net_profit_inc_for_all_years
        roe
        net_profit_per_liabilities
        working_capital_per_liabilities
        dividend_per_share
        avg_net_profit_inc_for_this_year
      ].freeze

      Contract ClassName[Share] => Any
      def initialize(share)
        @share = share
      end

      Contract Any
      def call
        return unless share_valid?

        analytics_result = share.company.analytics_results.find_or_initialize_by(date: share.date)
        analytics_result.assign_attributes(analytics_result_attributes)
        analytics_result.save!
      end

      private

      attr_reader :share

      def share_valid?
        analytics_ratio.present? && analytics_foundation.present?
      end

      def analytics_result_attributes
        @analytics_result_attributes ||= begin
          ATTRS.each_with_object({}) do |attr, result|
            result[attr] = send(attr)
          end.merge(date: share.date)
        end
      end

      def dividend_per_share
        @dividend_per_share ||= begin
          financial_position&.dividend_per_share
        end
      end

      def current_assets_per_current_liabilities
        @current_assets_per_current_liabilities ||= begin
          analytics_ratio.current_assets_per_current_liabilities
        end
      end

      def share_price_per_net_profit
        @share_price_per_net_profit ||= analytics_ratio.share_price_per_net_profit
      end

      def avg_net_profit_inc_for_this_year
        @avg_net_profit_inc_for_this_year ||= analytics_foundation.avg_net_profit_inc_for_this_year
      end

      def avg_net_profit_inc_for_all_years
        @avg_net_profit_inc_for_all_years ||= analytics_foundation.avg_net_profit_inc_for_all_years
      end

      def roe
        @roe ||= analytics_foundation.roe
      end

      def net_profit_per_liabilities
        @net_profit_per_liabilities ||= analytics_ratio.net_profit_per_liabilities
      end

      def working_capital_per_liabilities
        @working_capital_per_liabilities ||= analytics_ratio.working_capital_per_liabilities
      end

      def analytics_foundation
        @analytics_foundation ||= share.company.analytics_foundations.find_by(date: share.date)
      end

      def analytics_ratio
        @analytics_ratio ||= share.company.analytics_ratios.find_by(date: share.date)
      end

      def financial_position
        @financial_position ||= share.company.ifrs_financial_positions.find_by(date: share.date)
      end
    end
  end
end
