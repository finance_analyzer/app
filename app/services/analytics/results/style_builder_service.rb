# frozen_string_literal: true

# rubocop:disable Metrics/PerceivedComplexity
module Analytics
  module Results
    class StyleBuilderService < ApplicationService
      STYLES = {
        bad: 'bg-red',
        not_bad: 'bg-dark-yellow',
        normal: 'bg-yellow',
        good: 'bg-green',
      }.freeze

      Contract Symbol, Maybe[Num] => Any
      def initialize(key, value)
        @key = key
        @value = value
      end

      Contract String
      def call
        value.nil? ? '' : send(key)
      end

      private

      attr_reader :key, :value

      def current_assets_per_current_liabilities
        if value < 1
          STYLES[:bad]
        else
          STYLES[:good]
        end
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      def share_price_per_net_profit
        if value.negative? || value > 30
          STYLES[:bad]
        elsif value >= 0 && value <= 10
          STYLES[:good]
        elsif value > 10 && value <= 20
          STYLES[:not_bad]
        elsif value > 20 && value <= 30
          STYLES[:normal]
        end
      end

      def avg_net_profit_inc_for_all_years
        if value.negative? || value > 50
          STYLES[:bad]
        elsif value >= 0 && value <= 30
          STYLES[:good]
        elsif value > 30 && value <= 50
          STYLES[:normal]
        end
      end
      # rubocop:enable Metrics/CyclomaticComplexity

      def roe
        if value < 5
          STYLES[:bad]
        elsif value >= 5 && value < 9
          STYLES[:normal]
        else
          STYLES[:good]
        end
      end

      def net_profit_per_liabilities
        if value.negative?
          STYLES[:bad]
        elsif value >= 0 && value < 0.2
          STYLES[:not_bad]
        elsif value >= 0.2 && value < 0.4
          STYLES[:normal]
        else
          STYLES[:good]
        end
      end

      def working_capital_per_liabilities
        if value.negative?
          STYLES[:bad]
        elsif value >= 0 && value < 0.2
          STYLES[:not_bad]
        elsif value >= 0.2 && value < 0.4
          STYLES[:normal]
        else
          STYLES[:good]
        end
      end

      def dividend_per_share
        if value.nil? || value.blank? || value.zero?
          STYLES[:bad]
        else
          STYLES[:good]
        end
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      def avg_net_profit_inc_for_this_year
        if value.negative? || value > 50
          STYLES[:bad]
        elsif value >= 0 && value <= 30
          STYLES[:good]
        elsif value > 30 && value <= 50
          STYLES[:normal]
        end
      end
      # rubocop:enable Metrics/CyclomaticComplexity
    end
  end
end
# rubocop:enable Metrics/PerceivedComplexity
