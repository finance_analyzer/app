# frozen_string_literal: true

class BrowserService
  def self.call
    new.call
  end

  def call
    result = Capybara::Session.new(:cuprite, app)
    proxy(result)
    result
  end

  private

  def proxy(browser)
    return if ENV['PROXY_HOST'].nil? || ENV['PROXY_PORT'].nil?

    browser.driver.set_proxy(ENV['PROXY_HOST'], ENV['PROXY_PORT'], nil, nil, nil)
  end

  def app
    ->(_env) { [200, { 'Content-Type' => 'text/html' }, ['Hello World!']] }
  end
end
