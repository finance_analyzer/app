# frozen_string_literal: true

module Companies
  class SetConomyUrlService < ApplicationService
    Contract ClassName[Company] => Any
    def initialize(company)
      @company = company
    end

    Contract Any
    def call
      company.update!(company_params)
    ensure
      browser&.driver&.quit
    end

    private

    attr_reader :company

    def load_index
      browser.visit('https://www.conomy.ru/')
      sleep(2)
    end

    def company_params
      @company_params ||= {
        conomy_url: conomy_url,
      }
    end

    def conomy_url
      @conomy_url ||= begin
        load_index

        browser.fill_in('issuer_search', with: company.index)
        sleep(2)

        browser.find_all('div.awesomplete li', visible: false).first.click

        browser.current_url
      end
    end

    def browser
      @browser ||= BrowserService.call
    end
  end
end
