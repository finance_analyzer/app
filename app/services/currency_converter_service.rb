# frozen_string_literal: true

class CurrencyConverterService < ApplicationService
  class WrongResponseException < StandardError; end

  Contract String, String, Maybe[Date] => Any
  def initialize(from, to, date = nil)
    @from = from.upcase
    @to = to.upcase
    @date = date.nil? ? 'latest' : date.strftime('%Y-%m-%d')
  end

  Contract Num
  def call
    response = HTTParty.get(api_url)
    raise WrongResponseException unless response.ok?

    response.dig('rates', to)
  end

  private

  attr_reader :from, :to, :date

  def api_url
    @api_url ||= "https://api.exchangeratesapi.io/#{date}?base=#{from}"
  end
end
