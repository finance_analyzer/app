# frozen_string_literal: true

module Ifrs
  module ComprehensiveIncomes
    class ParserService < ApplicationService
      class WrongUrlsException < StandardError; end

      DATA_METHODS = {
        'Выручка' => :revenue,
        'Итого выручка от реализации' => :revenue,
        'Итого выручка от реализации и доход от совместных и ассоциированных предприятий' => :revenue,
        'Выручка от продаж' => :revenue,
        'Процентные доходы от операций лизинга' => :revenue,
        'Процентные доходы' => :revenue,
        'Выручка, нетто' => :revenue,
        'Прибыль/убыток' => :net_profit,
        'Чистая прибыль от непрерывной деятельности' => :net_profit,
        'Убыток за отчетный год' => :net_profit,
        'Прибыль за год' => :net_profit,
        'Прибыль за отчетный период от продолжающейся деятельности' => :net_profit,
        'Чистая прибыль' => :net_profit,
        'Убыток/прибыль за финансовый период' => :net_profit,
        'Чистый убыток/прибыль' => :net_profit,
      }.freeze

      Contract ClassName[Company], Maybe[Hash] => Any
      def initialize(company, start_calculating: false)
        @company = company
        @start_calculating = start_calculating
      end

      # rubocop:disable Metrics/MethodLength
      # rubocop:disable Metrics/AbcSize
      Contract Any
      def call
        Ifrs::ComprehensiveIncome.transaction do
          tables.each do |table|
            TableParserService.call(rows(table), data_methods, format_key(table)).each do |year_data|
              formatted_date = year_data[:date].to_date
              comprehensive_income = company.ifrs_comprehensive_incomes.find_or_initialize_by(date: formatted_date)
              comprehensive_income.assign_attributes(year_data.except(:date))
              comprehensive_income.save!
            rescue ArgumentError => e
              Raven.capture_exception(e)
            end
          end
        end
        company.perform_calculating if start_calculating
      ensure
        browser&.driver&.quit
      end
      # rubocop:enable Metrics/MethodLength
      # rubocop:enable Metrics/AbcSize

      private

      attr_reader :company, :start_calculating

      def data_methods
        @data_methods ||= begin
          DATA_METHODS.merge(
            Ifrs::ComprehensiveIncomes::DataMethod.to_hash
          )
        end
      end

      def tables
        @tables ||= html.css('div#page-container div.table')
      end

      def rows(table)
        table.css('table tbody tr')
      end

      def html
        @html ||= begin
          urls.each do |url|
            browser.visit(url)
            sleep(2)

            result = Nokogiri::HTML(browser.html)
            next if result.css('head').text.include?('Страница не найдена')

            return result
          end
          raise WrongUrlsException, "Comprehensive incomes, company id: #{company.id}"
        end
      end

      def urls
        @urls ||= [
          company.conomy_url + "/#{company.index.to_s.downcase}-ifrs-pu",
          company.conomy_url + '/eonr-ifrs-pu',
          company.conomy_url + '/epln-ifrs-pu'
        ]
      end

      def browser
        @browser ||= BrowserService.call
      end

      def format_key(table)
        table.css('div.comment span').text
      end
    end
  end
end
