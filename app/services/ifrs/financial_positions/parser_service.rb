# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    class ParserService < ApplicationService
      Contract ClassName[Company], Maybe[Hash] => Any
      def initialize(company, start_calculating: false)
        @company = company
        @start_calculating = start_calculating
      end

      Contract Any
      def call
        Ifrs::FinancialPosition.transaction do
          records_data.each do |record_data|
            financial_position = company.ifrs_financial_positions.find_or_initialize_by(date: record_data[:date])
            financial_position.assign_attributes(record_data.except(:date))
            financial_position.save!
          end
        end
        company.perform_calculating if start_calculating
      end

      private

      attr_reader :company, :start_calculating

      def records_data
        @records_data ||= begin
          general = TableParsers::GeneralService.call(company)
          dividends = TableParsers::DividendsService.call(company)
          dividends.last[:date] = general.last[:date]
          MergeArraysByFieldService.call(general, dividends, :date)
        end
      end
    end
  end
end
