# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    module TableParsers
      class DividendsService < ApplicationService
        class WrongUrlsException < StandardError; end

        Contract ClassName[Company] => Any
        def initialize(company)
          @company = company
        end

        Contract ArrayOf[Hash]
        def call
          ReverseTableParserService.call(rows, DATA_METHODS)
        ensure
          browser&.driver&.quit
        end

        private

        DATA_METHODS = {
          'column_1' => {
            attribute: :dividend_per_share,
            column: 1,
            attribute_format: ->(string) { string.gsub(',', '.') },
          },
        }.freeze

        attr_reader :company

        # rubocop:disable Metrics/MethodLength
        def rows
          @rows ||= begin
            urls.each do |url|
              browser.visit(url)
              sleep(2)

              html = Nokogiri::HTML(browser.html)
              begin
                return html.css('div#page-container table tbody')[2].css('tr')
              rescue NoMethodError => e
                Raven.capture_exception(e)
                nil
              end
            end
            raise WrongUrlsException, "FP dividends, company id: #{company.id}"
          end
        end
        # rubocop:enable Metrics/MethodLength

        def urls
          @urls ||= [
            company.conomy_url + "/#{company.index.to_s.downcase}div",
            company.conomy_url + "/#{company.index.to_s.downcase}-div",
            company.conomy_url + '/eonr-div'
          ]
        end

        def browser
          @browser ||= BrowserService.call
        end
      end
    end
  end
end
