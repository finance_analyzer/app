# frozen_string_literal: true

module Ifrs
  module FinancialPositions
    module TableParsers
      class GeneralService < ApplicationService
        include Conomy::Table

        class WrongUrlsException < StandardError; end

        DATA_METHODS = {
          'Итого внеоборотные активы' => :non_current_assets,
          'Итого долгосрочные активы' => :non_current_assets,
          'Итого нетекущие активы' => :non_current_assets,
          'Итого текущие активы' => :current_assets,
          'Итого оборотные активы' => :current_assets,
          'Итого краткосрочные активы' => :current_assets,
          'Итого активы' => :total_assets,
          'Итого активов' => :total_assets,
          'Итого капитал' => :equity,
          'Итого собственных средств' => :equity,
          'Итого акционерный капитал' => :equity,
          'Итого долгосрочные обязательства' => :non_current_liabilities,
          'Итого текущие обязательства' => :current_liabilities,
          'Итого краткосрочные обязательства' => :current_liabilities,
          'Итого обязательства' => :total_liabilities,
          'Итого обязательств' => :total_liabilities,
        }.freeze

        Contract ClassName[Company] => Any
        def initialize(company)
          @company = company
        end

        Contract ArrayOf[Hash]
        def call
          result_rows = []
          tables.each do |table|
            table_rows = TableParserService.call(rows(table), data_methods, format_key(table))
            table_rows.each do |row|
              result_rows << calculate_blank_fields(row)
            end
          end
          result_rows
        ensure
          browser&.driver&.quit
        end

        private

        attr_reader :company

        def data_methods
          @data_methods ||= begin
            DATA_METHODS.merge(
              Ifrs::FinancialPositions::DataMethod.to_hash
            )
          end
        end

        def calculate_blank_fields(row)
          result = row.dup
          if result[:total_liabilities].nil?
            result[:total_liabilities] = result[:non_current_liabilities].to_i + result[:current_liabilities].to_i
          end
          result
        end

        def tables
          @tables ||= html.css('div#page-container div.table')
        end

        def rows(table)
          table.css('table tbody tr')
        end

        def html
          @html ||= begin
            urls.each do |url|
              browser.visit(url)
              sleep(2)

              result = Nokogiri::HTML(browser.html)
              next if result.css('head').text.include?('Страница не найдена')

              return result
            end
            raise WrongUrlsException, "FP general, company id: #{company.id}"
          end
        end

        def urls
          @urls ||= [
            company.conomy_url + "/#{company.index.to_s.downcase}-ifrs-fp",
            company.conomy_url + '/eonr-ifrs-fp',
            company.conomy_url + '/epln-ifrs-fp'
          ]
        end

        def browser
          @browser ||= BrowserService.call
        end

        def format_key(table)
          table.css('div.comment span').text
        end
      end
    end
  end
end
