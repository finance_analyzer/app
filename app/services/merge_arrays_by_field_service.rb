# frozen_string_literal: true

class MergeArraysByFieldService < ApplicationService
  Contract ArrayOf[Hash], ArrayOf[Hash], Or[String, Symbol] => Any
  def initialize(array1, array2, field)
    @array1 = array1
    @array2 = array2
    @field = field
  end

  def call
    (array1.pluck(field) & array2.pluck(field)).map do |key|
      item1 = array1.find { |item| item[field] == key }
      item2 = array2.find { |item| item[field] == key }
      item1.merge(item2)
    end
  end

  private

  attr_reader :array1, :array2, :field
end
