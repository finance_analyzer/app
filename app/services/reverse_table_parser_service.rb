# frozen_string_literal: true

class ReverseTableParserService < ApplicationService
  Contract ClassName[Nokogiri::XML::NodeSet], Hash => Any
  def initialize(rows, data_methods)
    @rows = rows.to_a
    @data_methods = data_methods
  end

  Contract ArrayOf[Hash]
  def call
    rows[1..-1].each_with_index.each_with_object([]) do |(row, _row_index), items|
      columns = row.css('td').to_a

      date = format_date(columns.first.text)
      next if date.nil?

      items << columns.each_with_index.each_with_object(date: date) do |(column, column_index), item|
        data_method = data_methods["column_#{column_index}"]
        item[data_method[:attribute]] = format_attribute(column.text, data_method) if data_method
      end
    end
  end

  private

  attr_reader :rows, :data_methods

  def format_date(string)
    year = string[0..3].to_i
    return nil if year.zero?

    "31.12.#{year}"
  end

  def format_attribute(string, method_name)
    return string if method_name[:attribute_format].nil?

    method_name[:attribute_format].call(string)
  end
end
