# frozen_string_literal: true

module Shares
  module AttributeParsers
    class AmountService < ApplicationService
      class AIOColumnIsNotFoundException < StandardError; end
      class WrongUrlsException < StandardError; end

      DATA_METHODS = {
        'Количество акций, шт.' => :amount,
      }.freeze

      Contract ClassName[Company] => Any
      def initialize(company)
        @company = company
      end

      Contract Num
      def call
        columns = TableParserService.call(rows(html), DATA_METHODS)
        aoi_column = columns.find { |column| column[:date] == 'АОИ' }
        raise AIOColumnIsNotFoundException, columns.count if aoi_column.nil?

        aoi_column[:amount].to_i
      ensure
        browser&.driver&.quit
      end

      private

      attr_reader :company

      def rows(html_block)
        html_block.css('div#page-container .table_wrapper').last.css('table tbody tr')
      end

      def html
        @html ||= begin
          urls.each do |url|
            browser.visit(url)
            sleep(2)

            result = Nokogiri::HTML(browser.html)
            next if result.css('head').text.include?('Страница не найдена') || rows(result).blank?

            return result
          end
          raise WrongUrlsException, "FP general, company id: #{company.id}"
        end
      end

      def urls
        @urls ||= [
          company.conomy_url + "/#{company.index.to_s.downcase}-netasset",
          company.conomy_url + '/eonr-netasset'
        ]
      end

      def browser
        @browser ||= BrowserService.call
      end
    end
  end
end
