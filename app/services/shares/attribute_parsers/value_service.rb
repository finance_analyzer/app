# frozen_string_literal: true

module Shares
  module AttributeParsers
    class ValueService < ApplicationService
      class FinamEmIsNilException < StandardError; end

      Contract ClassName[Company], Num, Num => Any
      def initialize(company, start_year, end_year = now_year)
        @company = company
        @start_year = start_year
        @end_year = end_year
      end

      Contract Hash
      def call
        raise FinamEmIsNilException if em.nil?

        years.each_with_object({}) do |year, result|
          next if year == now_year && current_year_date.nil?

          date = year == now_year ? current_year_date : "31.12.#{year}"
          result[date] = response.scan(date_regex(year)).flatten.last
        end.compact
      end

      private

      attr_reader :company, :start_year, :end_year

      def date_regex(year)
        if year == now_year
          /#{year}#{current_year_date.month.to_s.rjust(2, '0')}\d{2};\d+;(\d+)/
        else
          /#{year}12\d{2};\d+;(\d+)/
        end
      end

      def current_year_date
        @current_year_date ||= begin
          [
            company.ifrs_financial_positions.to_a,
            company.ifrs_comprehensive_incomes.to_a
          ].flatten.find { |ifp| ifp.date.year == now_year }&.date
        end
      end

      def em
        @em ||= company.finam_em&.value
      end

      def years
        @years ||= (start_year..end_year).to_a
      end

      def response
        @response ||= HTTParty.get(url).to_s
      end

      def url
        @url ||= <<~HEREDOC.gsub("\n", '')
          http://export.finam.ru/shares.csv?market=1
          &em=#{em}
          &code=#{company.index}&apply=0
          &df=1&mf=0&yf=#{start_year}
          &from=01.01.#{start_year}
          &dt=31&mt=11&yt=#{end_year}
          &to=31.12.#{end_year}
          &p=8&f=shares&e=.csv
          &cn=#{company.index}
          &dtf=1&tmf=1&MSOR=1&mstime=on&mstimever=1&sep=3&sep2=1&datf=4&at=1
        HEREDOC
      end

      def now_year
        @now_year = Date.current.month >= 3 ? Date.current.year : Date.current.year - 1
      end
    end
  end
end
