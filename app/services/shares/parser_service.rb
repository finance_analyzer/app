# frozen_string_literal: true

module Shares
  class ParserService < ApplicationService
    Contract ClassName[Company], Maybe[Hash] => Any
    def initialize(company, start_calculating: false)
      @company = company
      @start_calculating = start_calculating
    end

    def call
      sync_shares
      company.perform_calculating if start_calculating
    end

    private

    attr_reader :company, :start_calculating

    def sync_shares
      Share.transaction do
        values.each do |date, value|
          share = company.shares.find_or_initialize_by(date: date)
          share.assign_attributes(
            amount: amount,
            value: value
          )
          share.save!
        end
      end
    end

    def years
      @years ||= (min_year..DateTime.current.year).to_a
    end

    def min_year
      @min_year ||= begin
        [
          Ifrs::ComprehensiveIncome.minimum(:date),
          Ifrs::FinancialPosition.minimum(:date),
          Date.parse('31.12.2013')
        ].min.year
      end
    end

    def amount
      @amount ||= AttributeParsers::AmountService.call(company)
    end

    def values
      @values ||= AttributeParsers::ValueService.call(company, years.first, years.last)
    end
  end
end
