# frozen_string_literal: true

class TableParserService < ApplicationService
  include Conomy::Table

  Contract ClassName[Nokogiri::XML::NodeSet], Hash, Maybe[String] => Any
  def initialize(rows, data_methods, format_key = nil)
    @rows = rows.to_a
    @data_methods = data_methods
    @format_key = format_key
  end

  Contract ArrayOf[Hash]
  def call
    years.each_with_index.each_with_object([]) do |(year, year_index), items|
      item = { date: year }
      rows.each do |row|
        columns = row.css('td').map(&:text)
        method_name = data_methods[format_column_name(columns.first)]
        method_value = columns[year_index + 1]
        item[method_name] = format_value(method_value.gsub(' ', ''), year) if method_name && method_value
      end
      items << item
    end
  end

  private

  attr_reader :rows, :data_methods, :format_key

  def years
    @years ||= begin
      rows.first.css('td').map(&:text)[1..-1]
    end
  end

  def format_value(value, date)
    (value.to_f * multiplier * exchange_rate(date)).to_s
  end

  def format_column_name(value)
    value.gsub("\n", ' ')
  end

  def multiplier
    @multiplier ||= begin
      return 1 if format_key.nil?

      result = MULTIPLIER_KEYS[format_key]
      raise MultiplierNotFound, format_key if result.nil?

      result
    end
  end

  # rubocop:disable Metrics/MethodLength
  def exchange_rate(date)
    return 1 if format_key.nil?

    currency = EXCHANGE_CONVERTER_CURRENCIES[format_key]
    raise ExchangeConverterCurrencyNotFound, format_key if currency.nil?

    if currency == 'RUB'
      1
    else
      CurrencyConverterService.call(currency, 'RUB', date.class == String ? Date.parse(date) : date)
    end
  rescue ArgumentError => e
    Raven.capture_exception(e)
    1
  end
  # rubocop:enable Metrics/MethodLength
end
