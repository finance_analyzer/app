# frozen_string_literal: true

module Analytics
  class CalculateAllWorker
    include Sidekiq::Worker

    def perform(company_id)
      company = Company.find(company_id)

      ::Analytics::CalculateAllService.call(company)
    end
  end
end
