# frozen_string_literal: true

module Companies
  class SetConomyUrlWorker
    include Sidekiq::Worker

    def perform(company_id)
      company = Company.find(company_id)

      SetConomyUrlService.call(company)
    end
  end
end
