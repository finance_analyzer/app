# frozen_string_literal: true

module Shares
  class ParserWorker
    include Sidekiq::Worker

    def perform(company_id, start_calculating = false)
      company = Company.find(company_id)

      ParserService.call(company, start_calculating: start_calculating)
    end
  end
end
