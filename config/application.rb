# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'
require 'capybara/cuprite'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FinanceAnalyzer
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.time_zone = 'Moscow'

    config.i18n.default_locale = :ru

    config.autoload_paths << Rails.root.join('lib')
    config.eager_load_paths << Rails.root.join('lib')

    Capybara.server = :webrick
    options = {
      headless: true,
      js_errors: false,
      window_size: [1400, 1400],
      browser_options: {
        'no-sandbox': nil,
      },
      timeout: 120,
      process_timeout: 120,
    }

    Capybara.register_driver :cuprite do |app|
      Capybara::Cuprite::Driver.new(app, options)
    end

    Capybara.javascript_driver = :cuprite
    Capybara.default_driver = :cuprite
  end
end
