# frozen_string_literal: true

module Contracts
  module Builtin
    # Takes a class +A+. If argument is object of type +A+, the contract passes.
    # If it is a subclass of A (or not related to A in any way), it fails.
    # Example: <tt>Exactly[Numeric]</tt>
    class ClassName < CallableClass
      def initialize(cls)
        @cls = cls
      end

      def valid?(val)
        val.class.name == @cls.name
      end

      def to_s
        "exactly #{@cls.name}"
      end
    end

    class ParentClassName < CallableClass
      def initialize(cls)
        @cls = cls
      end

      def valid?(val)
        val.class.name == @cls.name ||
          val.class.module_parents.map(&:name).include?(@cls.name) ||
          val.class.superclass.to_s == @cls.name
      end

      def to_s
        "exactly #{@cls.name}"
      end
    end
  end
end
