# frozen_string_literal: true

Loaf.configure do |config|
  config.match = :exact
end
