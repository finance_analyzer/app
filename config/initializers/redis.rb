# frozen_string_literal: true

REDIS = begin
  if ENV['REDIS_URL'].present?
    url = URI.parse(ENV['REDIS_URL'])
    Redis.new(host: url.host, port: url.port, db: 9)
  else
    Redis.new(host: ENV['REDIS_HOST'], port: ENV['REDIS_PORT'], db: ENV['REDIS_DB_INDEX'])
  end
        rescue StandardError
          Redis.new(host: '127.0.0.1', port: 6379, db: 9)
end
