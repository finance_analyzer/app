# frozen_string_literal: true

Raven.configure do |config|
  config.dsn = 'https://8ba383553ff14787b232829b98ba95ec:edda2e30ee694c39a3d15451b748d810@sentry.io/1864255'
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.environments = %w[production]
end
