# frozen_string_literal: true

# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  navigation.items do |primary|
    primary.item :companies, I18n.t('companies'), companies_path
    primary.item :total_analytics, I18n.t('total_analytics_name'), total_analytics_path
    primary.item :ci_data_methods, I18n.t('ci_data_methods'), ifrs_comprehensive_incomes_data_methods_path
    primary.item :fp_data_methods, I18n.t('fp_data_methods'), ifrs_financial_positions_data_methods_path
  end
end
