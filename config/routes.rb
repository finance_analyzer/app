# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  root 'total_analytics#index'

  namespace :ifrs do
    namespace :comprehensive_incomes do
      resources :data_methods, except: :show
    end

    namespace :financial_positions do
      resources :data_methods, except: :show
    end
  end

  resources :companies do
    post 'parse_and_calculate', to: 'companies#parse_and_calculate', as: 'parse_and_calculate'
    post 'calculate', to: 'companies#calculate', as: 'calculate'

    namespace :ifrs do
      resources :comprehensive_incomes, only: %i[index]
      post 'parse_comprehensive_incomes', to: 'comprehensive_incomes#parse', as: 'parse_comprehensive_incomes'

      resources :financial_positions, only: %i[index]
      post 'parse_financial_positions', to: 'financial_positions#parse', as: 'parse_financial_positions'
    end

    namespace :analytics do
      resources :foundations, only: %i[index]
      post 'calculate_foundations', to: 'foundations#calculate', as: 'calculate_foundations'

      resources :ratios, only: %i[index]
      post 'calculate_ratios', to: 'ratios#calculate', as: 'calculate_ratios'

      post 'calculate_results', to: 'results#calculate', as: 'calculate_results'
    end

    resources :shares, except: %i[show]
    post 'parse_shares', to: 'shares#parse', as: 'parse_shares'
  end

  get 'total_analytics', to: 'total_analytics#index', as: 'total_analytics'
  post 'calculate_total_analytics', to: 'total_analytics#calculate', as: 'calculate_total_analytics'
end
