process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

externals: {
  jquery: 'jQuery'
}

module.exports = environment.toWebpackConfig()
