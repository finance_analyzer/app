class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name, null: false
      t.string :index, limit: 5, null: false
      t.string :conomy_url

      t.timestamps
    end
  end
end
