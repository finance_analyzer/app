class CreateIfrsFinancialPositions < ActiveRecord::Migration[6.0]
  def change
    create_table :ifrs_financial_positions do |t|
      t.integer :non_current_assets, limit: 8, null: false
      t.integer :current_assets, limit: 8, null: false
      t.integer :total_assets, limit: 8, null: false

      t.integer :equity, limit: 8, null: false
      t.integer :non_current_liabilities, limit: 8, null: false
      t.integer :current_liabilities, limit: 8, null: false
      t.integer :total_liabilities, limit: 8, null: false

      t.float :dividend_per_share, null: false

      t.date :date, null: false

      t.references :company

      t.timestamps

      t.index %i[company_id date], unique: true
    end
  end
end
