class CreateIfrsComprehensiveIncomes < ActiveRecord::Migration[6.0]
  def change
    create_table :ifrs_comprehensive_incomes do |t|
      t.integer :revenue, limit: 8, null: false
      t.integer :net_profit, limit: 8, null: false

      t.date :date, null: false

      t.references :company

      t.timestamps

      t.index %i[company_id date], unique: true
    end
  end
end
