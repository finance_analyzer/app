class CreateShares < ActiveRecord::Migration[6.0]
  def change
    create_table :shares do |t|
      t.integer :amount, limit: 8, null: false
      t.float :value, null: false

      t.date :date, null: false

      t.references :company

      t.timestamps

      t.index %i[company_id date], unique: true
    end
  end
end
