class CreateAnalyticsFoundations < ActiveRecord::Migration[6.0]
  def change
    create_table :analytics_foundations do |t|
      t.float :roe, null: false
      t.integer :total_market_capitalization, limit: 8, null: false
      t.float :avg_net_profit_inc_for_this_year
      t.float :avg_net_profit_inc_for_all_years
      t.float :net_profit_per_share, null: false
      t.float :avg_net_profit_inc_per_share_for_this_year
      t.float :avg_net_profit_inc_per_share_for_all_years
      t.integer :working_capital, limit: 8, null: false
      t.float :balance_share_price, null: false

      t.date :date, null: false

      t.references :company

      t.timestamps

      t.index %i[company_id date], unique: true
    end
  end
end
