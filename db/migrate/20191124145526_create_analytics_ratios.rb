class CreateAnalyticsRatios < ActiveRecord::Migration[6.0]
  def change
    create_table :analytics_ratios do |t|
      t.float :share_price_per_net_profit, null: false
      t.float :share_price_per_balance_price, null: false
      t.float :net_profit_per_revenue, null: false
      t.float :net_profit_per_balance_price, null: false
      t.float :working_capital_per_liabilities, null: false
      t.float :market_capitalization_per_revenue, null: false
      t.float :current_assets_per_current_liabilities, null: false
      t.float :net_profit_per_liabilities, null: false

      t.date :date, null: false

      t.timestamps

      t.references :company

      t.index %i[company_id date], unique: true
    end
  end
end
