class CreateIndustries < ActiveRecord::Migration[6.0]
  def change
    create_table :industries do |t|
      t.string :name, null: false

      t.timestamps

      t.index :name, unique: true
    end
  end
end
