class CreateIfrsComprehensiveIncomesDataMethods < ActiveRecord::Migration[6.0]
  def change
    create_table :ifrs_comprehensive_incomes_data_methods do |t|
      t.string :key, null: false
      t.string :value, null: false

      t.timestamps
    end
  end
end
