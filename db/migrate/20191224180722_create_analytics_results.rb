class CreateAnalyticsResults < ActiveRecord::Migration[6.0]
  def change
    create_table :analytics_results do |t|
      t.date :date, null: false
      t.float :current_assets_per_current_liabilities, null: false
      t.float :share_price_per_net_profit, null: false
      t.float :avg_net_profit_inc_for_all_years
      t.float :roe, null: false
      t.float :net_profit_per_liabilities, null: false
      t.float :working_capital_per_liabilities, null: false

      t.timestamps

      t.references :company, foreign_key: true

      t.index %i[company_id date], unique: true
    end
  end
end
