class AddFinamEmToCompanies < ActiveRecord::Migration[6.0]
  def self.up
    add_reference :companies, :finam_em, index: true

    Company.transaction do
      Company.where.not(finam_em_name: nil).each do |company|
        finam_em = Finam::Em.find_by(name: company.finam_em_name)
        next if finam_em.nil?

        company.finam_em = finam_em
        company.save
      end
    end

    remove_column :companies, :finam_em_name
  end

  def self.down
    add_column :companies, :finam_em_name, :string

    Company.transaction do
      Company.where.not(finam_em: nil).each do |company|
        company.finam_em_name = company.finam_em.name
        company.save!
      end
    end

    remove_column :companies, :finam_em_id
  end
end
