class AddUniqIndexesToCompanies < ActiveRecord::Migration[6.0]
  def change
    add_index :companies, :name, unique: true
    add_index :companies, :index, unique: true
  end
end
