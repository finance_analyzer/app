class AddDividendPerShareToAnalyticsResults < ActiveRecord::Migration[6.0]
  def change
    add_column :analytics_results, :dividend_per_share, :float
  end
end
