class AddAvgNetProfitIncForThisYearToAnalyticsResults < ActiveRecord::Migration[6.0]
  def change
    add_column :analytics_results, :avg_net_profit_inc_for_this_year, :float
  end
end
