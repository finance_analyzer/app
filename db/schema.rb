# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_28_183352) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "analytics_foundations", force: :cascade do |t|
    t.float "roe", null: false
    t.bigint "total_market_capitalization", null: false
    t.float "avg_net_profit_inc_for_this_year"
    t.float "avg_net_profit_inc_for_all_years"
    t.float "net_profit_per_share", null: false
    t.float "avg_net_profit_inc_per_share_for_this_year"
    t.float "avg_net_profit_inc_per_share_for_all_years"
    t.bigint "working_capital", null: false
    t.float "balance_share_price", null: false
    t.date "date", null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id", "date"], name: "index_analytics_foundations_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_analytics_foundations_on_company_id"
  end

  create_table "analytics_ratios", force: :cascade do |t|
    t.float "share_price_per_net_profit", null: false
    t.float "share_price_per_balance_price", null: false
    t.float "net_profit_per_revenue", null: false
    t.float "net_profit_per_balance_price", null: false
    t.float "working_capital_per_liabilities", null: false
    t.float "market_capitalization_per_revenue", null: false
    t.float "current_assets_per_current_liabilities", null: false
    t.float "net_profit_per_liabilities", null: false
    t.date "date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "company_id"
    t.index ["company_id", "date"], name: "index_analytics_ratios_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_analytics_ratios_on_company_id"
  end

  create_table "analytics_results", force: :cascade do |t|
    t.date "date", null: false
    t.float "current_assets_per_current_liabilities", null: false
    t.float "share_price_per_net_profit", null: false
    t.float "avg_net_profit_inc_for_all_years"
    t.float "roe", null: false
    t.float "net_profit_per_liabilities", null: false
    t.float "working_capital_per_liabilities", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "company_id"
    t.float "dividend_per_share"
    t.float "avg_net_profit_inc_for_this_year"
    t.index ["company_id", "date"], name: "index_analytics_results_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_analytics_results_on_company_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
    t.string "index", limit: 5, null: false
    t.string "conomy_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "country_id", null: false
    t.bigint "industry_id", null: false
    t.bigint "finam_em_id"
    t.index ["country_id"], name: "index_companies_on_country_id"
    t.index ["finam_em_id"], name: "index_companies_on_finam_em_id"
    t.index ["index"], name: "index_companies_on_index", unique: true
    t.index ["industry_id"], name: "index_companies_on_industry_id"
    t.index ["name"], name: "index_companies_on_name", unique: true
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_countries_on_name", unique: true
  end

  create_table "finam_ems", force: :cascade do |t|
    t.string "name", null: false
    t.string "value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ifrs_comprehensive_incomes", force: :cascade do |t|
    t.bigint "revenue", null: false
    t.bigint "net_profit", null: false
    t.date "date", null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id", "date"], name: "index_ifrs_comprehensive_incomes_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_ifrs_comprehensive_incomes_on_company_id"
  end

  create_table "ifrs_comprehensive_incomes_data_methods", force: :cascade do |t|
    t.string "key", null: false
    t.string "value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "ifrs_financial_positions", force: :cascade do |t|
    t.bigint "non_current_assets", null: false
    t.bigint "current_assets", null: false
    t.bigint "total_assets", null: false
    t.bigint "equity", null: false
    t.bigint "non_current_liabilities", null: false
    t.bigint "current_liabilities", null: false
    t.bigint "total_liabilities", null: false
    t.float "dividend_per_share", null: false
    t.date "date", null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id", "date"], name: "index_ifrs_financial_positions_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_ifrs_financial_positions_on_company_id"
  end

  create_table "ifrs_financial_positions_data_methods", force: :cascade do |t|
    t.string "key", null: false
    t.string "value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "industries", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_industries_on_name", unique: true
  end

  create_table "shares", force: :cascade do |t|
    t.bigint "amount", null: false
    t.float "value", null: false
    t.date "date", null: false
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id", "date"], name: "index_shares_on_company_id_and_date", unique: true
    t.index ["company_id"], name: "index_shares_on_company_id"
  end

  add_foreign_key "analytics_results", "companies"
  add_foreign_key "companies", "countries"
  add_foreign_key "companies", "industries"
end
