seed_file = Rails.root.join('db', 'seeds', 'production', 'countries.seeds.yml')
data = YAML.load_file(seed_file)

p('countries...')
data.each do |item|
  record = Country.find_or_initialize_by(id: item.delete('id'))
  record.assign_attributes(item)
  record.save!
end
