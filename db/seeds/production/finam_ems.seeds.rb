data = []
4.times do |time|
  data += JSON.load(Rails.root.join('db', 'seeds', 'production', "finam_ems_#{time + 1}.seeds.json"))
end

p('finam_ems...')
Finam::Em.transaction do
  data.each do |item|
    record = Finam::Em.find_or_initialize_by(value: item[1]['content'])
    record.assign_attributes({name: item[2]['content']})
    record.save!
  end
end
