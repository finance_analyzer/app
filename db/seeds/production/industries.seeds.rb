seed_file = Rails.root.join('db', 'seeds', 'production', 'industries.seeds.yml')
data = YAML.load_file(seed_file)

p('industries...')
data.each do |item|
  record = Industry.find_or_initialize_by(id: item.delete('id'))
  record.assign_attributes(item)
  record.save!
end
