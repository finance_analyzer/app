# frozen_string_literal: true

module Conomy
  module Table
    class MultiplierNotFound < StandardError; end
    class ExchangeConverterCurrencyNotFound < StandardError; end

    MULTIPLIER_KEYS = {
      'В тыс. руб.' => 1_000,
      'В млн руб.' => 1_000_000,
      'В тыс. долл. США' => 1_000,
      'В тыс. долл США' => 1_000,
      'В млн долл. США' => 1_000_000,
    }.freeze

    EXCHANGE_CONVERTER_CURRENCIES = {
      'В тыс. руб.' => 'RUB',
      'В млн руб.' => 'RUB',
      'В тыс. долл. США' => 'USD',
      'В тыс. долл США' => 'USD',
      'В млн долл. США' => 'USD',
    }.freeze
  end
end
