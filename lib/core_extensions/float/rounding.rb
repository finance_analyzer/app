# frozen_string_literal: true

module CoreExtensions
  module Float
    module Rounding
      def take(number)
        return nil if nil? || nan?

        to_f.to_s.match(/\d+\.\d{1,#{number}}/)[0].to_f
      end
    end
  end
end
