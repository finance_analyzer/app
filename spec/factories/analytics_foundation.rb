require 'faker'

FactoryBot.define do
  factory :analytics_foundation, class: 'Analytics::Foundation' do
    roe { Faker::Number.number(digits: 2) }
    total_market_capitalization { Faker::Number.number(digits: 6) }
    avg_net_profit_inc_for_this_year { Faker::Number.number(digits: 2) }
    avg_net_profit_inc_for_all_years { Faker::Number.number(digits: 2) }
    net_profit_per_share { Faker::Number.number(digits: 2) }
    avg_net_profit_inc_per_share_for_this_year { Faker::Number.number(digits: 2) }
    avg_net_profit_inc_per_share_for_all_years { Faker::Number.number(digits: 2) }
    working_capital { Faker::Number.number(digits: 6) }
    balance_share_price { Faker::Number.number(digits: 2) }
    date { '31.12.2018' }
    company
  end
end
