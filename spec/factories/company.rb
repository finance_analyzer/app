require 'faker'

FactoryBot.define do
  factory :company do
    name { Faker::Company.name }
    index { Faker::Alphanumeric.alpha(number: 4) }
    country
    industry
    finam_em
  end
end
