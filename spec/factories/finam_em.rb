require 'faker'

FactoryBot.define do
  factory :finam_em, class: 'Finam::Em' do
    name { 'Name' }
    value { 'Value' }
  end
end
