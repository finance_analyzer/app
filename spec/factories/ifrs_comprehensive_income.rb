require 'faker'

FactoryBot.define do
  factory :ifrs_comprehensive_income, class: 'Ifrs::ComprehensiveIncome' do
    revenue { Faker::Number.number(digits: 10) }
    net_profit { Faker::Number.number(digits: 10) }
    date { '31.12.2018' }
    company
  end
end
