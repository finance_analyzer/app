require 'faker'

FactoryBot.define do
  factory :ifrs_financial_position, class: 'Ifrs::FinancialPosition' do
    non_current_assets { Faker::Number.number(digits: 10) }
    current_assets { Faker::Number.number(digits: 10) }
    total_assets { Faker::Number.number(digits: 10) }
    equity { Faker::Number.number(digits: 10) }
    non_current_liabilities { Faker::Number.number(digits: 10) }
    current_liabilities { Faker::Number.number(digits: 10) }
    total_liabilities { Faker::Number.number(digits: 10) }
    dividend_per_share { Faker::Number.number(digits: 2) }
    date { '31.12.2018' }
    company
  end
end
