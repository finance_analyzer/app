require 'faker'

FactoryBot.define do
  factory :industry do
    name { Faker::Name.name }
  end
end
