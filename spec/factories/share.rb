require 'faker'

FactoryBot.define do
  factory :share do
    amount { Faker::Number.number(digits: 8) }
    value { Faker::Number.number(digits: 3) }
    date { '31.12.2018' }
    company
  end
end
