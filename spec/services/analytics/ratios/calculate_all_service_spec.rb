# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Analytics::Ratios::CalculateAllService', type: :service do
  describe 'Call method' do
    Float.include CoreExtensions::Float::Rounding

    before(:each) do
      @csv_data_helper = CsvDataHelper.new
      @check_keys = %i[
        share_price_per_net_profit
        share_price_per_balance_price
        net_profit_per_revenue
        net_profit_per_balance_price
        working_capital_per_liabilities
        market_capitalization_per_revenue
        current_assets_per_current_liabilities
        net_profit_per_liabilities
        date
      ]
      @round_keys = @check_keys[0..-2]
    end

    it 'Test with csv data 1' do
      csv_file_name = 'test_data_1'
      company = create(:company)

      @csv_data_helper.read_data('share', csv_file_name, company.id).each do |object|
        create(:share, object)
      end
      @csv_data_helper.read_data('ifrs_comprehensive_income', csv_file_name, company.id).each do |object|
        create(:ifrs_comprehensive_income, object)
      end
      @csv_data_helper.read_data('ifrs_financial_position', csv_file_name, company.id).each do |object|
        create(:ifrs_financial_position, object)
      end
      @csv_data_helper.read_data('analytics_foundation', csv_file_name, company.id).each do |object|
        create(:analytics_foundation, object)
      end

      expected_analytics_ratios = @csv_data_helper.read_data('analytics_ratio', csv_file_name, company.id)

      Analytics::Ratios::CalculateAllService.call(company)

      aggregate_failures('Testing database') do
        expect(Analytics::Ratio.all.count).to(eq(6))
        (2013..2018).to_a.each do |year|
          expect(
            format_hash(
              company.analytics_ratios.find_by(date: "#{year}-12-31")
                                      &.as_json
                                      &.symbolize_keys
                                      &.slice(*@check_keys)
            )
          ).to(
            eq(
              format_hash(
                expected_analytics_ratios.find { |object| object[:date] == "#{year}-12-31" }
                                         .symbolize_keys
                                         .slice(*@check_keys)
              )
            )
          )
        end
      end
    end

    it 'Test with csv data 2' do
      csv_file_name = 'test_data_2'
      company = create(:company)

      @csv_data_helper.read_data('share', csv_file_name, company.id).each do |object|
        create(:share, object)
      end
      @csv_data_helper.read_data('ifrs_comprehensive_income', csv_file_name, company.id).each do |object|
        create(:ifrs_comprehensive_income, object)
      end
      @csv_data_helper.read_data('ifrs_financial_position', csv_file_name, company.id).each do |object|
        create(:ifrs_financial_position, object)
      end
      @csv_data_helper.read_data('analytics_foundation', csv_file_name, company.id).each do |object|
        create(:analytics_foundation, object)
      end

      expected_analytics_ratios = @csv_data_helper.read_data('analytics_ratio', csv_file_name, company.id)

      Analytics::Ratios::CalculateAllService.call(company)

      aggregate_failures('Testing database') do
        expect(Analytics::Ratio.all.count).to(eq(6))
        (2013..2018).to_a.each do |year|
          expect(
            format_hash(
              company.analytics_ratios.find_by(date: "#{year}-12-31")
                                      &.as_json
                                      &.symbolize_keys
                                      &.slice(*@check_keys)
            )
          ).to(
            eq(
              format_hash(
                expected_analytics_ratios.find { |object| object[:date] == "#{year}-12-31" }
                                         .symbolize_keys
                                         .slice(*@check_keys)
              )
            )
          )
        end
      end
    end

    private

    def format_hash(hash)
      return nil if hash.nil?

      @round_keys.each do |key|
        hash[key] = hash[key].take(2) unless hash[key].nil?
      end

      hash
    end
  end
end
