require 'csv'

class CsvDataHelper
  KEYS = {
    amount: 'share',
    value: 'share',
    revenue: 'ifrs_comprehensive_income',
    net_profit: 'ifrs_comprehensive_income',
    non_current_assets: 'ifrs_financial_position',
    current_assets: 'ifrs_financial_position',
    total_assets: 'ifrs_financial_position',
    equity: 'ifrs_financial_position',
    non_current_liabilities: 'ifrs_financial_position',
    current_liabilities: 'ifrs_financial_position',
    total_liabilities: 'ifrs_financial_position',
    dividend_per_share: 'ifrs_financial_position',
    roe: 'analytics_foundation',
    total_market_capitalization: 'analytics_foundation',
    avg_net_profit_inc_for_this_year: 'analytics_foundation',
    avg_net_profit_inc_for_all_years: 'analytics_foundation',
    net_profit_per_share: 'analytics_foundation',
    avg_net_profit_inc_per_share_for_this_year: 'analytics_foundation',
    avg_net_profit_inc_per_share_for_all_years: 'analytics_foundation',
    working_capital: 'analytics_foundation',
    balance_share_price: 'analytics_foundation',
    share_price_per_net_profit: 'analytics_ratio',
    share_price_per_balance_price: 'analytics_ratio',
    net_profit_per_revenue: 'analytics_ratio',
    net_profit_per_balance_price: 'analytics_ratio',
    working_capital_per_liabilities: 'analytics_ratio',
    market_capitalization_per_revenue: 'analytics_ratio',
    current_assets_per_current_liabilities: 'analytics_ratio',
    net_profit_per_liabilities: 'analytics_ratio',
  }.stringify_keys

  def read_data(data_type, file_name, company_id = nil)
    result = {}

    CSV.foreach(Rails.root.join('spec', 'support', 'data', "#{file_name}.csv")) do |row|
      if row[3] == 'year'
        row[4..-1].each_with_index do |year, index|
          result[year] = {_index: index + 4}
        end
      elsif KEYS[row[3]] == data_type
        result.each do |key, value|
          value[row[3]] = format_value(row[value[:_index]], row[1])
        end
      end
    end

    result.map { |year, value|
      item = value.except(:_index).merge(
        {
          company_id: company_id,
          date: "#{year}-12-31"
        }.compact
      )
      yield(item) if block_given?
      item
    }
  end

  private

  def format_value(value, format)
    return nil if value.nil?

    result = value.gsub(',', '.').gsub(/ |%/, '')
    result = result&.send(format) if format.present?
    result
  end
end
